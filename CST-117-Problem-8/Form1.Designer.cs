﻿namespace CST_117_Problem_8
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.fatIn = new System.Windows.Forms.TextBox();
            this.carbIn = new System.Windows.Forms.TextBox();
            this.convertBtn = new System.Windows.Forms.Button();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.carbOut = new System.Windows.Forms.TextBox();
            this.fatOut = new System.Windows.Forms.TextBox();
            this.SuspendLayout();
            // 
            // fatIn
            // 
            this.fatIn.Location = new System.Drawing.Point(12, 31);
            this.fatIn.Name = "fatIn";
            this.fatIn.Size = new System.Drawing.Size(100, 20);
            this.fatIn.TabIndex = 0;
            // 
            // carbIn
            // 
            this.carbIn.Location = new System.Drawing.Point(12, 72);
            this.carbIn.Name = "carbIn";
            this.carbIn.Size = new System.Drawing.Size(100, 20);
            this.carbIn.TabIndex = 1;
            // 
            // convertBtn
            // 
            this.convertBtn.Location = new System.Drawing.Point(118, 31);
            this.convertBtn.Name = "convertBtn";
            this.convertBtn.Size = new System.Drawing.Size(75, 61);
            this.convertBtn.TabIndex = 2;
            this.convertBtn.Text = "CONVERT";
            this.convertBtn.UseVisualStyleBackColor = true;
            this.convertBtn.Click += new System.EventHandler(this.ConvertBtn_Click);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(9, 15);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(58, 13);
            this.label1.TabIndex = 3;
            this.label1.Text = "Fat Grams:";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(9, 56);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(65, 13);
            this.label2.TabIndex = 4;
            this.label2.Text = "Carb Grams:";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(196, 56);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(99, 13);
            this.label3.TabIndex = 6;
            this.label3.Text = "Calories from carbs:";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(196, 15);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(85, 13);
            this.label4.TabIndex = 5;
            this.label4.Text = "Calories from fat:";
            // 
            // carbOut
            // 
            this.carbOut.Location = new System.Drawing.Point(199, 72);
            this.carbOut.Name = "carbOut";
            this.carbOut.ReadOnly = true;
            this.carbOut.Size = new System.Drawing.Size(100, 20);
            this.carbOut.TabIndex = 8;
            // 
            // fatOut
            // 
            this.fatOut.Location = new System.Drawing.Point(199, 31);
            this.fatOut.Name = "fatOut";
            this.fatOut.ReadOnly = true;
            this.fatOut.Size = new System.Drawing.Size(100, 20);
            this.fatOut.TabIndex = 7;
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(320, 105);
            this.Controls.Add(this.carbOut);
            this.Controls.Add(this.fatOut);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.convertBtn);
            this.Controls.Add(this.carbIn);
            this.Controls.Add(this.fatIn);
            this.Name = "Form1";
            this.Text = "Calorie Converter";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.TextBox fatIn;
        private System.Windows.Forms.TextBox carbIn;
        private System.Windows.Forms.Button convertBtn;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.TextBox carbOut;
        private System.Windows.Forms.TextBox fatOut;
    }
}

