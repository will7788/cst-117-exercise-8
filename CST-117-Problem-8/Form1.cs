﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace CST_117_Problem_8
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        private void ConvertBtn_Click(object sender, EventArgs e)
        {
            double fatGrams;
            double carbGrams;

            if (Double.TryParse(fatIn.Text, out fatGrams))
            {
                fatOut.Text = fromFat(fatGrams) + "";
            }
            else if (fatIn.Text.Equals(""))
            {
                fatOut.Text = "";
            }
            if (Double.TryParse(carbIn.Text, out carbGrams))
            {
                carbOut.Text = fromCarb(carbGrams) + "";
            }
            else if (carbIn.Text.Equals(""))
            {
                carbOut.Text = "";
            }
        }

        private double fromFat(double x) { return x * 9; }

        private double fromCarb(double y) { return y * 4; }
    }
}
